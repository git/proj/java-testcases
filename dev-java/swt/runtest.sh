#!/bin/bash
if [[ ${#} != 1 ]]; then
	echo "Usage: $0 <swt-slot>"
	echo "Example: $0 3.4"
	exit
fi

SLOT="${1}"

echo "compiling..."
[[ ! -e Snippet128.class ]] && javac -classpath $(java-config -p swt-${SLOT}) Snippet128.java

export CLASSPATH="."
gjl_package=swt-${SLOT}
gjl_main="Snippet128"
echo "Running..."
GJL_DEBUG=1
source /usr/share/java-config-2/launcher/launcher.bash
