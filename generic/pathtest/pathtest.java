class pathtest {

	public static void main (String[] args) {
		System.out.println("CLASSPATH: " + System.getProperty("java.class.path"));
		System.out.println("BOOTCLASSPATH: " + System.getProperty("sun.boot.class.path"));
		System.out.println("LIBRARY_PATH: " + System.getProperty("java.library.path"));
	}
}
