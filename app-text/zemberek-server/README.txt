IMPORTANT: Test contains non-ascii characters
---------------------------------------------

issue
dbus-send --print-reply --system --dest='net.zemberekserver.server.dbus' /net/zemberekserver/server/dbus/ZemberekDbus net.zemberekserver.server.dbus.IZemberek.oner string:'yalnış'
on commandline.

The output should be similar to the following (Excat results may change due to changes in wordlist):
method return sender=:1.4 -> dest=:1.29 reply_serial=2
   array [
      string "yalnız"
      string "yalanış"
      string "yalmış"
      string "yanlış"
   ]

